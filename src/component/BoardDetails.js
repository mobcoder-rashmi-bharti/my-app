import React from 'react'
import { Table , Card } from 'react-bootstrap'

export default function BoardDetails() {
    return (
        <div>
            <h2>Board 1</h2>
            <div className="main"> 
            <Card>
          
            <Table striped bordered hover size="lg">
  <thead>
    <tr>
      <th>TO DO</th>
      <th>In Progress</th>
      <th>On Hold</th>
      <th>Completed</th>
      <th>Released</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Task 1</td>
      <td>Task 2</td>
      <td>Task 3</td>
      <td>Task 4</td>
    </tr>
    <tr>
      <td>Task 1</td>
      <td>Task 2</td>
      <td>Task 3</td>
      <td>Task 4</td>
    </tr>
    <tr>
      <td>Task 1</td>
      <td>Task 2</td>
      <td>Task 3</td>
      <td>Task 4</td>
         
    </tr>
    <tr>
      <td>Task 1</td>
      <td>Task 2</td>
      <td>Task 3</td>
      <td>Task 4</td>
         
    </tr>
  </tbody>
</Table>
                    
        </Card>
          </div>
          
        </div>
    )
}
