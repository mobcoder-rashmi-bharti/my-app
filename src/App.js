import logo from './logo.svg';
import './App.css';
import Login from './component/Login';
import SignUp from './component/SignUp'
import 'bootstrap/dist/css/bootstrap.min.css';
import CreateBoard from './component/CreateBoard';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import BoardDetails from './component/BoardDetails';

// import './App.scss'


function App() {
  return (
    <div className="App">
       <Router>
        <Routes>

          <Route exact  path='/' element={<SignUp />} > </Route>
          <Route exact  path='/Login' element={<Login />}></Route>
          <Route exact  path='/CreateBoard' element={<CreateBoard />}></Route>

     </Routes>
</Router> 

{/* <BoardDetails /> */}

    </div>
  );
}

export default App;
